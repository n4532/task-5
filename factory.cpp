#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "factory.h"
#include "string.h"

using namespace std;

Factory::Factory()
{
}

Factory::Factory(char* name, char* production, int shares, int costShares)
{
	strcpy(factoryName, name);
	strcpy(mainProduction, production);
	this->costOneShare = costShares;
	this->sharesNumber = shares;
}

void Factory::setManagerName(char* name)
{
	strcpy(managerName, name);
}

void Factory::setProduction(char* production)
{
	strcpy(mainProduction, production);
}

void Factory::displayFactory()
{
	cout << "It is the " << this->factoryName << "." << endl;
	cout << "This factory is managed by " << this->managerName << endl;
	cout << this->mainProduction << " is produced here" << endl;
	cout << "The capitalization of the factory is " << this->getÑapitalization() << endl;
}

int Factory::getСapitalization()
{
	return this->costOneShare * this->sharesNumber;
}
