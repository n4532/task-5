#pragma once

const int N = 256;

class Factory
{
public: 
	Factory();
	Factory(char*, char*, int, int);
	void setManagerName(char*);
	void setProduction(char*);
	void displayFactory();
	int get�apitalization();
private:
	char factoryName[256];
	char managerName[256];
	char mainProduction[256];
	int costOneShare = 1;
	int sharesNumber = 100;
};

