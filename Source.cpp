#include <iostream>
#include "factory.h"

using namespace std;

int main() {
	const int N = 20;
	char name[N] = "MTZ";
	char production[N] = "Traktory";
	char manager[N] = "Pavel";
	int shares = 200000;
	int cost = 200;
	Factory mtz(name, production, shares, cost);
	mtz.setManagerName(manager);

	mtz.displayFactory();

}